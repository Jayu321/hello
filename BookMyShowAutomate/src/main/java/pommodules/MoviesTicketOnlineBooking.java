package pommodules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MoviesTicketOnlineBooking {
	WebDriver driver;
	@FindBy (xpath="(//div[text()='Hindi'])[2]")
	private WebElement Languages;
	
    @FindBy (xpath="(//div[@class='sc-133848s-2 sc-1y4pbdw-8 jUeUxF'])[1]")
	private WebElement Genres;
    
    @FindBy (xpath="(//div[@class='sc-133848s-2 sc-1y4pbdw-12 cUNqTx'])[9]")
	private WebElement ActionMovie;
	
	@FindBy (xpath="//div[@class='sc-133848s-2 sc-1y4pbdw-8 jUeUxF']")
    private WebElement MovieFormat;
	
	@FindBy (xpath="(//div[text()='2D'])[2]")
    private WebElement Movie2D;
	
	@FindBy (xpath="((//div[@class='sc-133848s-3 sc-133848s-5 gqBECX'])[2]//div)[20]")
    private WebElement ClickOnMovie;
	
    private Actions act;
	public  MoviesTicketOnlineBooking(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
		this.driver= driver;
		act=new Actions(driver);
	}
	

	public void selectLanguage()
	{
		Languages.click();
	}
	public void ClickOnGenres()
	{
		act.moveToElement(ClickOnMovie).click().perform();
	}
	
	public void SelectActionFromGenres()
	{
		ActionMovie.click();
	}
	
	public void ClickOnFormateofMovie()
	{
		MovieFormat.click();
	}
	
	public void Select2DFormateOfMovie()
	{
		Movie2D.click();
	}
	
	public void ClickOndecidedMovie()
	{
		ClickOnMovie.click();
	}
	
	
	
	
}
