package pommodules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class pathanmoviereviews {
	
	@FindBy(xpath="(//div[@class='sc-11q8erw-0 cPZFLW']//div)[23]")
	private WebElement BookTicket;
	
	@FindBy(xpath="(//div[@class='sc-1k6uqqy-0 gZyDCA']//div)[7]")
	private WebElement TWODmovie;
	
	
	
	public pathanmoviereviews(WebDriver driver)
	
	{
		PageFactory.initElements(driver, this);
	}
	
	public void ClickonBookticketOption()
	{
		BookTicket.click();
	}
	
	public void select2Dmovie()
	{
		TWODmovie.click();
	}
	
	
}
