package pommodules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Pathanmovie2dSHOWTIMEANDonlinebooking {
	
	@FindBy(xpath="((//li[@class=\"list\"])[7]//div)[22]")
	private WebElement Movietiming ;
	
	@FindBy(xpath="(//div[@id='tnc']//div)[8]")
	private WebElement TrmsCondition;
	
	@FindBy(xpath="//div[@id='proceed-Qty']")
	private WebElement SelectSeat;


	public Pathanmovie2dSHOWTIMEANDonlinebooking(WebDriver driver)
	
	{
		PageFactory.initElements(driver, this);
	}
	
	public void selectmovietiming()
	{
		Movietiming.click();
	}

	public void AcceptTermAndCondition()
	{
		TrmsCondition.click();
	}
	
	public void Selectseat()
	{
		SelectSeat.click();
	}
}
