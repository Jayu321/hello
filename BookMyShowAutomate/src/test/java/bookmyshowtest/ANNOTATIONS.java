package bookmyshowtest;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ANNOTATIONS {
	
	@BeforeTest
	public void print()
	{
		System.out.println("BeforeTest");
	}
	
	@BeforeClass
	public void print2()
	{
		System.out.println("BeforeClass");
	}
	
	@BeforeMethod
	public void print3()
	{
		System.out.println("@BeforeMethod");
	}
	
	@Test
	public void print4()
	{
		System.out.println("@test=4");
	}
	
	@Test
	public void print5()
	{
		System.out.println("@test=5");
	}
	
	@Test
	public void print6()
	{
		System.out.println("@test=6");
	}
	
@AfterClass
	
	public void print8()
	{
		System.out.println("@afterclass");
	}
	
	@AfterMethod
	public void print7()
	{
		System.out.println("@AfterMethod");
	}
	
	
	@AfterTest
	public void print9()
	{
		System.out.println("@@AfterTest");
	}
	
	

}
